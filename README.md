# Base ignition module

Base setup for CoreOS, check vars.tf for all input variables.

* Automatically generated SSH key for "core" user.
* Default password "coreos" set.
* OpenSSH config that disables PasswordAuthentication.
* Timezone set.
* Helpful podman aliases.
