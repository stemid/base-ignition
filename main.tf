resource "tls_private_key" "sshkey" {
  algorithm = "ED25519"
}

data "ignition_user" "core" {
  name = "core"
  home_dir = "/var/home/core"
  ssh_authorized_keys = [
    "${tls_private_key.sshkey.public_key_openssh}"
  ]
  password_hash = var.password_hash
}

data "ignition_link" "timezone" {
  path = "/etc/localtime"
  target = "../usr/share/zoneinfo/${var.timezone}"
}

data "ignition_file" "aliases" {
  path = "/etc/profile.d/aliases.sh"
  content {
    content = file("${path.module}/templates/aliases.sh")
  }
  mode = 420
}

data "ignition_config" "config" {
  users = [
    data.ignition_user.core.rendered
  ]

  files = [
    data.ignition_file.aliases.rendered
  ]

  links = [
    data.ignition_link.timezone.rendered
  ]
}
