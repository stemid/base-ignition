terraform {
  required_providers {
    ignition = {
      source = "registry.terraform.io/community-terraform-providers/ignition"
      version = "<=2.3.5"
    }
    tls = {
      source = "registry.terraform.io/hashicorp/tls"
      version = "4.0.4"
    }
  }
}
