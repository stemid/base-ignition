alias aws="podman run --rm -i -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -e AWS_S3_BUCKET -e AWS_DEFAULT_REGION docker.io/amazon/aws-cli"
alias jq="podman run --rm -i ghcr.io/jqlang/jq:1.7"
alias podman-system-generator="/usr/lib/systemd/system-generators/podman-system-generator"
alias vim="vi"
