output "config" {
  value = data.ignition_config.config
}

output "sshkey" {
  value = tls_private_key.sshkey
}
