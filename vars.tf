variable "timezone" {
  type = string
  description = "Default timezone: Europe/Copenhagen"
  default = "Europe/Copenhagen"
}

variable "password_hash" {
  type = string
  description = "Default password: coreos"
  default = "$6$xyz$p404G4c3q.3U11ZX0srzNDnEJhVhvNywuffNmMBh.Xd2hz8tQDhB3DWwRjFiQD8HnwVm3UGPArV1bEOlc4qgK1"
}

variable "keyboard_layout" {
  type = string
  default = "sv"
}
